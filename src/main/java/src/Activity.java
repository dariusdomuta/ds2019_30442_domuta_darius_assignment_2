package src;

import java.time.LocalDateTime;

public class Activity {
    private int patient_id;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activityLabel;

    public Activity() {
    }

    public Activity(LocalDateTime startTime, LocalDateTime endTime, String activityLabel) {
        this.patient_id = hashCode();
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityLabel = activityLabel;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }

    public int getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
