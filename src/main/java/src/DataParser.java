package src;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.Date;

public class DataParser {

    private static final String insertStatement = "INSERT INTO activity (patient_id,startTime,endTime,activityLabel)" +
            " VALUES (?,?,?,?)";

    private static final String deleteAllStatement = "DELETE FROM activity";

    private static Activity stringToObject(String string) {
        String[] tokens;
        tokens = string.split("\t\t");
        Activity activity = new Activity();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        activity.setStartTime(LocalDateTime.parse(tokens[0], formatter));
        activity.setEndTime(LocalDateTime.parse(tokens[1], formatter));
        activity.setActivityLabel(tokens[2].trim());
        activity.setPatient_id(activity.hashCode());
        return activity;
    }

    private static List<Activity> getData() {
        String fileName = "E:\\DS 2019\\activity.txt";
        List<Activity> activityList = new ArrayList<Activity>();
        Activity activity = new Activity();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(line -> activityList.add(stringToObject(line)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return activityList;
    }

    public static void insertActivityToDb(Activity activity) {

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setInt(1, activity.getPatient_id());
            preparedStatement.setString(2, activity.getStartTime().toString());
            preparedStatement.setString(3, activity.getEndTime().toString());
            preparedStatement.setString(4, activity.getActivityLabel());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection);
        }

    }

    public static String printMonitoredData() throws IOException {
        BufferedWriter bw = null;
        FileWriter fw = null;
        fw = new FileWriter("E:\\DS 2019\\activityParsed.txt");
        bw = new BufferedWriter(fw);

        StringBuilder sb = new StringBuilder();

        List<Activity> activityList = getData();

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(deleteAllStatement, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection);
        }

        for (Activity activity : activityList) {
            insertActivityToDb(activity);
            checkActivity(activity);
            sb.append(activity.getStartTime() + "       " + activity.getEndTime() + "      " + activity.getActivityLabel() + "\n");
            bw.write(activity.getStartTime() + "       " + activity.getEndTime() + "      " + activity.getActivityLabel() + "\n");
        }


        if (bw != null)
            bw.close();

        if (fw != null)
            fw.close();

        return sb.toString();
    }

    public static void checkActivity(Activity activity) {
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date start_time = null;
        Date end_time = null;

        try {
            start_time = formatter.parse(activity.getStartTime().toString().replace("T", " "));
            end_time = formatter.parse(activity.getEndTime().toString().replace("T", " "));
            long diff = end_time.getTime() - start_time.getTime();
            int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
            int diffhours = (int) (diff / (60 * 60 * 1000));
            int diffmin = (int) (diff / (60 * 1000));
            if((diffhours >= 12 || diffDays >= 1 || diffmin >= 720) && activity.getActivityLabel().equals("Sleeping") ){
                System.out.println("R1: The sleep period longer than 12 hours");
            }else if((diffhours >= 12 || diffDays >= 1 || diffmin >= 720) && activity.getActivityLabel().equals("Leaving") ){
                System.out.println("R2: The leaving activity (outdoor) is longer than 12 hours");
            }else if(diffmin >= 60 && (activity.getActivityLabel().equals("Toileting") || activity.getActivityLabel().equals("Showering") )){
                System.out.println("R3: The period spent in bathroom is longer than 1 hour");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
